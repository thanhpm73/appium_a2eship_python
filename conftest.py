import pytest
from utils.driver_config import DriverConfig
import os
import logging
@pytest.fixture(scope="function")
def driver():
    driver = DriverConfig.get_driver()
    yield driver
    driver.quit()

@pytest.fixture(scope="function")
def screenshot_on_pass(request, driver):
    yield
    if request.node.rep_call.passed:
        test_name = request.node.name
        screenshot_dir = "screenshots"
        if not os.path.exists(screenshot_dir):
            os.makedirs(screenshot_dir)
        driver.save_screenshot(f"{screenshot_dir}/{test_name}_pass.png")

@pytest.fixture(scope="function")
def screenshot_on_fail(request, driver):
    yield
    if request.node.rep_call.failed:
        test_name = request.node.name
        screenshot_dir = "screenshots"
        if not os.path.exists(screenshot_dir):
            os.makedirs(screenshot_dir)
        driver.save_screenshot(f"{screenshot_dir}/{test_name}_fail.png")

@pytest.hookimpl(tryfirst=True, hookwrapper=True)
def pytest_runtest_makereport(item, call):
    outcome = yield
    rep = outcome.get_result()
    setattr(item, "rep_" + rep.when, rep)

