# -*- coding: utf-8 -*-
import time
import pytest
from pages.login_page import LoginPage
from pages.add_job_page import AddPage
from pages.search_page import SearchPage
class TestSearchOrder:
    @pytest.mark.sanity
    @pytest.mark.run(order=3)
    def test_search_order_card(self, driver, screenshot_on_fail):
        login_page = LoginPage(driver)
        login_page.login("thanhpm", "123456")
        search_page = SearchPage(driver)
        search_page.search_order_card("123456")

    @pytest.mark.sanity
    @pytest.mark.run(order=4)
    def test_search_order_waiting(self, driver, screenshot_on_fail):
        login_page = LoginPage(driver)
        login_page.login("thanhpm", "123456")
        search_page = SearchPage(driver)
        search_page.search_order_waiting("1111222222")

    @pytest.mark.sanity
    @pytest.mark.run(order=5)
    def test_search_order_all(self, driver, screenshot_on_fail):
        login_page = LoginPage(driver)
        login_page.login("thanhpm", "123456")
        search_page = SearchPage(driver)
        search_page.search_order_all("33333")

    @pytest.mark.sanity
    @pytest.mark.run(order=6)
    def test_search_order_complete(self, driver, screenshot_on_fail):
        login_page = LoginPage(driver)
        login_page.login("thanhpm", "123456")
        search_page = SearchPage(driver)
        search_page.search_order_complete("9999")
