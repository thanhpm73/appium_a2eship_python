# -*- coding: utf-8 -*-
import time
import pytest
from pages.login_page import LoginPage
from pages.add_job_page import AddPage
class TestLogin:
    @pytest.mark.sanity
    @pytest.mark.run(order=1)
    def test_login_successful(self, driver, screenshot_on_fail):
        login_page = LoginPage(driver)
        login_page.login("thanhpm", "123456")
        time.sleep(2)
        assert login_page.get_text_home() =='Trang chủ', 'Login failer'

    @pytest.mark.sanity
    @pytest.mark.run(order=2)
    def test_login_failed(self, driver, screenshot_on_fail):
        login_page = LoginPage(driver)
        login_page.login("thanhpm11", "123456")
        time.sleep(2)
        assert login_page.get_text_login_failed() == 'Tài khoản đăng nhập không tồn tại hoặc mật khẩu không đúng'

    @pytest.mark.sanity
    @pytest.mark.run(order=3)
    def test_login_form_ui(self, driver, screenshot_on_fail):
        login_page = LoginPage(driver)
        assert login_page.is_username_field_displayed(), "Username field is not displayed"
        assert login_page.is_password_field_displayed(), "Password field is not displayed"
        assert login_page.is_login_button_displayed(), "Login button is not displayed"
