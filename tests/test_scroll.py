# -*- coding: utf-8 -*-
import time

import pytest
from pages.login_page import LoginPage
from pages.scroll_page import PageScroll
class TestScroll:

    def test_scroll_down(self, driver):
        login_page = LoginPage(driver)
        login_page.login("0967834766", "123456")

        scroll_page = PageScroll(driver)
        scroll_page.click_notification_button()
        time.sleep(3)
        scroll_new = PageScroll(driver)
        scroll_new.scroll_to_bottom()

    def test_scroll_up(self, driver):
        login_page = LoginPage(driver)
        login_page.login("0967834766", "123456")
        scroll_page = PageScroll(driver)
        scroll_page.click_notification_button()
        scroll_new = PageScroll(driver)
        time.sleep(2)
        scroll_new.scroll_to_top()



