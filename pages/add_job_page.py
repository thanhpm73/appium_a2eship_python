from appium.webdriver.common.appiumby import AppiumBy
from .base_page import BasePage
class AddPage(BasePage):

    btnAdd=(AppiumBy.XPATH, '//android.widget.TextView[@text="Thêm việc"]')
    btnCreateOrder = (AppiumBy.XPATH, '//android.widget.Button[@text="TẠO ĐƠN HÀNG"]')
    element_can_scroll = (AppiumBy.ID, 'vn.advn.trackingclientapp:id/layout_optional')
    def click_Add_button(self):
        self.click_element(self.btnAdd)

