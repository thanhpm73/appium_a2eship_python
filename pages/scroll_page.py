from appium.webdriver.common.appiumby import AppiumBy
from .base_page import BasePage
from appium.webdriver.common.touch_action import TouchAction


class PageScroll(BasePage):
    notification = (AppiumBy.ANDROID_UIAUTOMATOR, 'new UiSelector().resourceId("vn.advn.trackingclientapp:id/icon").instance(2)')
    element_need_scroll = (AppiumBy.ANDROID_UIAUTOMATOR, 'new UiSelector().text("T23254826617").instance(0)')

    def click_notification_button(self):
        self.click_element(self.notification)

    def scroll_to_bottom(self):
        # Giả sử cần cuộn 3 lần để đến cuối trang
        for _ in range(5):
            self.scroll_down()

    def scroll_to_top(self):
        for _ in range(2):  # Số lần cuộn có thể điều chỉnh
            self.scroll_up()
