import time

from appium.webdriver.common.appiumby import AppiumBy
from appium.webdriver.common.touch_action import TouchAction
from .base_page import BasePage


class SearchPage(BasePage):
    text_search = (AppiumBy.ANDROID_UIAUTOMATOR, 'new UiSelector().className("android.widget.EditText")')
    btn_order_waiting = (AppiumBy.ACCESSIBILITY_ID, 'Đơn hàng đang chờ')
    btn_all_order=(AppiumBy.ACCESSIBILITY_ID,'Tất cả')
    btn_order_complete=(AppiumBy.ACCESSIBILITY_ID,'Đã hoàn thành')
    def click_textbox_search(self):
        self.click_element(self.text_search)

    def click_order_waiting(self):
        self.click_element(self.btn_order_waiting)

    def click_btn_all_order(self):
        self.click_element(self.btn_all_order)
    def click_btn_order_complete(self):
        self.click_element(self.btn_order_complete)
    def input_text_search(self, txtsearch):
        self.input_text(self.text_search, txtsearch)

    def search_order_card(self, txtsearch):
        self.click_textbox_search()
        self.input_text_search(txtsearch)

    def search_order_waiting(self, txtsearch):
        time.sleep(2)
        self.click_order_waiting()
        self.click_textbox_search()
        self.input_text_search(txtsearch)

    def search_order_all(self, txtsearch):
        self.click_btn_all_order()
        self.click_textbox_search()
        self.input_text_search(txtsearch)

    def search_order_complete(self, txtsearch):
        self.click_btn_all_order()
        self.click_btn_order_complete()
        self.click_textbox_search()
        self.input_text_search(txtsearch)
