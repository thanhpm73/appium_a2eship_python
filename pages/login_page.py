import time

from appium.webdriver.common.appiumby import AppiumBy
from .base_page import BasePage
from selenium.webdriver.common.by import By


class LoginPage(BasePage):
    btn_start = (AppiumBy.ACCESSIBILITY_ID, 'Bắt đầu')
    username_field = (AppiumBy.ANDROID_UIAUTOMATOR, 'new UiSelector().className("android.widget.EditText").instance(0)')
    password_field = (AppiumBy.ANDROID_UIAUTOMATOR, 'new UiSelector().className("android.widget.EditText").instance(1)')
    btn_login = (AppiumBy.ANDROID_UIAUTOMATOR, 'new UiSelector().description("Đăng nhập").instance(1)')
    text_home = (AppiumBy.ACCESSIBILITY_ID, 'Trang chủ')
    text_login_failed=(AppiumBy.ACCESSIBILITY_ID,'Tài khoản đăng nhập không tồn tại hoặc mật khẩu không đúng')

    def enter_username(self, username):
        self.input_text(self.username_field, username)

    def enter_password(self, password):
        self.input_text(self.password_field, password)

    def click_start_button(self):
        self.click_element(self.btn_start)

    def click_username(self):
        self.click_element(self.username_field)
    def click_password(self):
        self.click_element(self.password_field)

    def click_login_button(self):
        self.click_element(self.btn_login)

    # def get_error_message(self):
    #     return self.get_text(self.ERROR_MESSAGE)
    def get_text_home(self):
        return self.get_attribute(self.text_home)
    def get_text_login_failed(self):
        return self.get_attribute(self.text_login_failed)

    def login(self, username, password):
        self.click_start_button()
        self.click_username()
        self.enter_username(username)
        time.sleep(2)
        self.click_password()
        self.enter_password(password)
        self.click_login_button()

    #Test UI
    def is_username_field_displayed(self):
        self.click_start_button()
        return self.is_element_displayed(self.username_field)

    def is_password_field_displayed(self):
        return self.is_element_displayed(self.password_field)
    def is_login_button_displayed(self):
        return self.is_element_displayed(self.btn_login)