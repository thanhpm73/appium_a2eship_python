from appium import webdriver
from appium.options.android import UiAutomator2Options
class DriverConfig:
    #may ao
    # @staticmethod
    # def get_driver():
    #     options = UiAutomator2Options()
    #     options.platform_name = 'Android'
    #     options.automation_name = 'uiautomator2'
    #     options.device_name = 'Android Emulator'
    #     options.app_package = 'vn.advn.trackingclientapp'
    #     options.app_activity = '.activities.MainActivity'
    #     return webdriver.Remote('http://localhost:2743', options=options)

    #may that
    @staticmethod
    def get_driver():
        options = UiAutomator2Options()
        options.platform_name = 'Android'
        options.automation_name = 'uiautomator2'
        options.device_name = 'HZQL1849DAL10800193'
        options.app_package = 'com.example.a2eship'
        options.app_activity = '.MainActivity'
        return webdriver.Remote('http://localhost:2744', options=options)
